"Author: Blaise Delaney, PhD Student, Cavendish Lab, Cambridge
"Sources: https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/, https://github.com/skwp/dotfiles/blob/master/vimrc
"Use: git clone Vundle Repo and run the command :PluginInstall; 
"Notes: YCM works with updated version of vim 

"preliminaries
"-------------
set nocompatible
filetype off  "required for Vundle 


"include Vundle in runtime path and Vundle setup
"-----------------------------------------------
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'

"<body of plugins>
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'jnurmine/Zenburn'
Plugin 'kien/ctrlp.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'ervandew/supertab'
"ASK CHRIS HOW TO UPDATE VIM - Plugin 'Valloric/YouCompleteMe'

"end of Vundle plugin management
call vundle#end()            

filetype plugin indent on    " required
let python_highlight_all=1
syntax on
set encoding=utf-8

"basic cosmetics (mostly self-explanatory)
"-----------------------------------------
set number
set mouse=a
set cmdheight=2                 "more infor on dispaly
set ruler                       "coordinates of cursos
set backspace=indent,eol,start  "Allow backspace in insert mode
set whichwrap+=<,>,h,l          "Allow for navigation via intuitive keys
set history=1000                "Store lots of :cmdline history
set showcmd                     "Show incomplete cmds down the bottom
set showmode                    "Show current mode down the bottom
set autoread                    "Reload files changed outside vim
set autoindent
set smartindent
set smarttab
set tabstop=4 shiftwidth=4 expandtab
set nostartofline               "mainatin column when jumping thorugh lines

"disable sounds and flashing for errors
"--------------------------------------
set visualbell
set t_vb=

"diplay funky chars and no wrap
"------------------------------
"set list listchars=tab:\ \ ,trail:·
set nowrap       "Don't wrap lines
set linebreak    "Wrap lines at convenient points

"autocomplete
"------------
"tab autocomplete menu
set wildmenu
set wildmode=longest:full,full

"search: incremental, highlight result, ignore case unless explicitly request
"Capital
"------
set incsearch
set hlsearch
set ignorecase
set smartcase

"handle buffer properly and ask to confirm before exit
"-----------------------------------------------------
set hidden
set confirm

"pasting (prevent vim from autoindenting when pasting from out)
"--------------------------------------------------------------
set pastetoggle=<F1> "paste mode before and after pasting

"status bar (replaced by plugin) 
"----------
"set laststatus=2
"set statusline=
"set statusline+=%#PmenuSel#
"set statusline+=%#LineNr#
"set statusline+=\ %F
"set statusline+=%m\
"set statusline+=%=
"set statusline+=%#CursorColumn#
"set statusline+=\ %y
"set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
"set statusline+=\ %{&fileformat}
"set statusline+=\ %p%%
"set statusline+=\ %l:%c
"set statusline+=\


"I think it's wait until the command is completed
set notimeout ttimeout ttimeoutlen=200

"kill all swaps
"--------------
set noswapfile
set nobackup
set nowb

"colours: numbers and cursorline
"-------------------------------
colorscheme zenburn
"highlight LineNr ctermfg=Black  ctermbg=LightCyan
"set cursorline
"hi Cursorline  ctermbg=black guibg=yellow gui=bold
